<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>HTML Form</title>
</head>
<body>

<form action="simpandata.php" method="POST">
    
<table border="0">
	<tr>
		<td><label for="nama">Nama Lengkap : </label></td>
		<td><input type="text" name="nama"></td>
	</tr>
	<tr>
		<td><label for="username">Username : </label></td>
		<td><input type="text" name="username"></td>
	</tr>
	<tr>
		<td>Email</td>
		<td><input type="email" name="email" placeholder="Enter your Email"></td>
	</tr>
	<tr>
		<td ><label for="textarea">Alamat : </label></td>
		<td><textarea name="alamat"></textarea></td>
	</tr>
	<tr>
		<td><label for="jeniskelamin">Jenis kelamin : </label></td>
		<td>
			<input type="radio" name="kelamin" value="pria">
			<label for="pria">pria</label>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="radio" name="kelamin" value="wanita">
			<label for="wanita">wanita</label>
		</td>
	</tr>
	<tr>
		<td>Hobi</td>
		<td><input type="checkbox" name="badminton">Badminton</td>
	</tr>
	<tr>
		<td></td>
		<td><input type="checkbox" name="berenang">Berenang</td>
	</tr>
	<tr>
		<td></td>
		<td><input type="checkbox" name="game">Bermain Game</td>
	</tr>
	<tr>
		<td ><label for="pekerjaan"></label>Pekerjaan : </td>
		<td>
			<select name="pekerjaan">
				<option value="-">Pilih</option>
				<option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
				<option value="Pegawai">Pegawai</option>
				<option value="Wirausaha">Wirausaha</option>
				<option value="Belum Bekerja">Belum Bekerja</option>
			</select>
		</td>
	</tr>
	<tr>
		<td  colspan="2"><button type="submit">Kirim!</button></td>
	</tr>
</table>
</form>

</body>
</html>