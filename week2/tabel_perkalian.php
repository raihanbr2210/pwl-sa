<!DOCTYPE html>
<!--Raihan Basithu Rahman 
    A11.2021.13912 / 4S02 -->
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabel Perkalian</title>
</head>
<body>

<!-- Form Tabel Perkalian -->
<form align="center" action="tabel_perkalian.php" method="POST">
    Input Nilai Angka
    <input type="text" name="nilai">
    <button type="submit" >Submit</button>
</form>

    <table align="center" border="2" cellspacing="0" cellpadding="10">
        <?php 
            if(isset($_POST['nilai'])) {
                $nilai = $_POST['nilai'];
            }
            echo $nilai;
            for($a=1; $a <= $nilai; $a++){
                echo "<tr>";
                for($n=1; $n <= $nilai; $n++){
                    echo "<td> $a x $n </td>";
                }
            }
        ?>
    </table>

</body>
</html>