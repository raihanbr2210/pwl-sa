<?php
session_start();
if (empty($_SESSION['cart']["arrCart"]))
    $_SESSION['cart']["arrCart"] = array();
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>RAIHANSHOP</title>

</head>
<body class="d-flex h-100 text-center flex-column">

    <!-- Navbar -->
    <nav id="navbar-top" class="navbar navbar-expand-md navbar-light bg-warning mb-5">
        <a class="navbar-brand ms-5 fs-2 p-4 fw-bold" href="#">RaihanShop</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample04">
            <ul class="navbar-nav ms-5 me-5 fs-4">
                <li class="nav-item active">
                    <a class="nav-link" href="#home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="cart-disp.php">Cart</a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- End Navbar -->

    <h2 class="mt-4 fw-bold">JUAL TANAMAN HIAS</h2>

    <!-- Product -->
    <div class="row d-flex justify-content-center row-cols-1 row-cols-md-2 g-4 mt-2">
        <div class="card bg-light shadow col-sm-4" style="width: 15rem; right: 5rem;">
            <img src='aglonemaAyunindi.jpg'  class="card-img-top" style="margin-top: 0.5rem;">
            <div class="card-body">
                <h5 class="card-title">Aglonema Ayunindi</h5>
                <p class="card-text"> Detail Produk:  
                    <br> Tanaman hias
                    <br> 20-30 cm
                    <br> 250 gram
                    <br> Ready
                </p>
                <p class="lead bg-info">Rp. 45000</p>
                <a href="addCart.php?brg=Aglonema Ayunindi&hrg=45000&jml=1" class="btn btn-success">Order</a>
            </div>
        </div>
        <div class="card bg-light shadow col-sm-4" style="width: 15rem; right: 2.5rem;">
            <img src='aglonemaBigroy.jpg'  class="card-img-top" style="margin-top: 0.5rem;">
            <div class="card-body">
                <h5 class="card-title">Aglonema Bigroy</h5>
                <p class="card-text">Detail Produk: 
                    <br> Tanaman hias
                    <br> 10-30 cm
                    <br> 200 gram
                    <br> Ready
                </p>
                <p class="lead bg-info">Rp. 60000</p>
                <a href="addCart.php?brg=Aglonema Bigroy&hrg=60000&jml=1" class="btn btn-success">Order</a>
            </div>
        </div>
        <div class="card bg-light shadow col-sm-4" style="width: 15rem;">
            <img src='aglonemaKhocin.jpg'  class="card-img-top" style="margin-top: 0.5rem;">
            <div class="card-body">
                <h5 class="card-title">Aglonema Khocin</h5>
                <p class="card-text">Detail Produk:
                    <br> Tanaman hias
                    <br> 10-35 cm
                    <br> 200 gram
                    <br> Ready
                </p>
                </p>
                <p class="lead bg-info">Rp. 65000</p>
                <a href="addCart.php?brg=Aglonema Khocin&hrg=65000&&jml=1" class="btn btn-success">Order</a>
            </div>
        </div>
        <div class="card bg-light shadow col-sm-4" style="width: 15rem; left: 2rem;">
            <img src='aglonemaRedAnjamaniDewasa.jpg'  class="card-img-top" style="margin-top: 0.5rem;">
            <div class="card-body">
                <h5 class="card-title">Aglonema Red Anjamani Dewasa</h5>
                <p class="card-text">Detail Produk:
                    <br> Tanaman hias
                    <br> 8-30 cm
                    <br> 150 gram
                    <br> Ready
                </p>
                <p class="lead bg-info">Rp. 70000</p>
                <a href="addCart.php?brg=Aglonema Red Anjamani Dewasa&hrg=70000&jml=1" class="btn btn-success">Order</a>
            </div>
        </div>
    </div>
    <!-- End Product-->

    <!--footer-->
    <footer class="bg-warning fw-bold text-dark mt-5">
        <div class="text-center p-5">
             RaihanShop © 2022 Copyright
        </div>
    </footer>
    <!--end footer-->

    <!-- Add JS Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
</body>

</html>
